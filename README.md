# Threema + PyGTK3
## THIS PROJECT IS NOT AFFILIATED TO Threema GmbH in ANY way.

Im just using the code they released to try to code an GTK+3 app.

This project uses submodules.

## <a name="license"></a>License

Threema for Android is licensed under the GNU Affero General Public License v3.

    Copyright (c) 2013-2020 Threema GmbH

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License, version 3,
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

Threema + PyGTK3 is licensed under the GNU Affero General Public License v3 or later.
    
    Copyright 2020, Ralf Rachinger

    Current license as of time of writing:
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License, version 3,
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


The full license text can be found in [`LICENSE`](LICENSE).
